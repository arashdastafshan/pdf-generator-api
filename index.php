<?php

use ArashDastafshan\PdfGeneratorApi\Silex\ControllerProvider\Home\HomeControllerProvider;
use ArashDastafshan\PdfGeneratorApi\Silex\ControllerProvider\UrlToPdf\UrlToPdfControllerProvider;
use Igorw\Silex\ConfigServiceProvider;

require_once __DIR__.'/vendor/autoload.php';

// get environment (prod or dev)
$env = getenv('APP_ENV') ?: 'prod';

$app = new Silex\Application();

// load global config files
$app->register(new ConfigServiceProvider(__DIR__.'/config/global/default.json'));
$app->register(new ConfigServiceProvider(__DIR__.'/config/global/'.$env.'.json'));

// load local config files (these need to be created first)
if (file_exists(__DIR__.'/config/local/default.json')) {
    $app->register(new ConfigServiceProvider(__DIR__.'/config/local/default.json'));
}
if (file_exists(__DIR__.'/config/local/'.$env.'.json')) {
    $app->register(new ConfigServiceProvider(__DIR__.'/config/local/'.$env.'.json'));
}

// set error handling
if (true === $app['debug']) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

// load validators
$app->register(new Silex\Provider\ValidatorServiceProvider());

// mount controllers
$app->mount('/', new HomeControllerProvider());
$app->mount('/url-to-pdf', new UrlToPdfControllerProvider());

$app->run();
