Feature: URL to PDF
  In order to have PDFs of websites
  As a user
  I want to be able to generate a PDF by giving a URL

  Scenario: Convert URL to PDF
    Given I have the URL "http://www.warnerbros.com/archive/spacejam/movie/jam.htm"
    When I convert the URL to PDF
    Then I should have created the same PDF as "space-jam-homepage.pdf"

  Scenario: Trying to convert invalid URL to PDF
    Given I have the URL "this is not an url"
    When I convert the URL to PDF
    Then I should get the error "The given URL is not valid."