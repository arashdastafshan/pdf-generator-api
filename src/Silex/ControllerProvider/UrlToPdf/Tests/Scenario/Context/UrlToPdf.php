<?php

namespace ArashDastafshan\PdfGeneratorApi\Silex\ControllerProvider\UrlToPdf\Tests\Scenario\Context;

use ArashDastafshan\PdfGeneratorApi\Silex\ControllerProvider\UrlToPdf\UrlToPdfControllerProvider;
use ArashDastafshan\PdfGeneratorApi\Silex\Tests\Scenario\Context\App;
use ArashDastafshan\UrlToPdfConverter\UrlToPdfConverter\PhpWkHtmlToPdfConverter;
use mikehaertl\wkhtmlto\Pdf;

/**
 * Defines application features from the specific context.
 */
class UrlToPdf extends App
{
    protected $url;
    protected $savePath;
    protected $error;

    /**
     * @param string $url
     * @Given I have the URL :url
     */
    public function iHaveTheUrl(string $url)
    {
        $this->url = $url;
        $this->savePath = $this->app['url_to_pdf_folder'].'/wkhtmltopdf-scenariotesting-'.md5($this->url).'.pdf';
    }

    /**
     * @When I convert the URL to PDF
     */
    public function iConvertTheUrlToPdf()
    {
        $wkHtmlToPdf = new Pdf($this->generatePdfOptions());

        $urlToPdfConverter = new PhpWkHtmlToPdfConverter(
            $this->url,
            $this->savePath,
            $wkHtmlToPdf
        );

        try {
            $controllerProvider = new UrlToPdfControllerProvider();
            if (true === $controllerProvider->validateUrlToPdfConverter($this->app, $urlToPdfConverter)) {
                $urlToPdfConverter->execute();
            }
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
        }
    }

    /**
     * @param string $filename
     * @Then I should have created the same PDF as :filename
     */
    public function iShouldHaveCreatedTheSamePdfAs(string $filename)
    {
        $filePath = dirname(__DIR__).'/assets/'.$filename;

        // Apparently, the file size will be exactly the same, but hash won't be.
        \PHPUnit_Framework_Assert::assertEquals(
            filesize($filePath),
            filesize($this->savePath)
        );
    }

    /**
     * @param string $error
     * @Then I should get the error :error
     */
    public function iShouldGetTheError(string $error)
    {
        \PHPUnit_Framework_Assert::assertSame($error, $this->error);
    }

    /**
     * Generate default options for PHP WkHtmlToPdf.
     *
     * @return array
     */
    private function generatePdfOptions()
    {
        return [
            'no-outline',
            'margin-top' => 0,
            'margin-right' => 0,
            'margin-bottom' => 0,
            'margin-left' => 0,
            'disable-smart-shrinking',
        ];
    }
}
