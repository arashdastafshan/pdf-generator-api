<?php

namespace ArashDastafshan\PdfGeneratorApi\Silex\ControllerProvider\UrlToPdf;

use ArashDastafshan\UrlToPdfConverter\UrlToPdfConverter\PhpWkHtmlToPdfConverter;
use ArashDastafshan\UrlToPdfConverter\UrlToPdfConverter\UrlToPdfConverter;
use mikehaertl\wkhtmlto\Pdf;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UrlToPdfControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        // creates a new controller based on the default route
        $controllers = $app['controllers_factory'];

        $controllers->get('/', function (Application $app, Request $request) {
            $url = $request->get('url');

            return $this->urlToPdf(
                $app,
                $url,
                $app['url_to_pdf_folder'].'/wkhtmltopdf-'.md5($url.microtime()).'.pdf'
            );
        });

        return $controllers;
    }

    /**
     * Perform the action.
     *
     * @param Application $app
     * @param string      $url
     * @param string      $savePath
     *
     * @return string|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    private function urlToPdf(Application $app, string $url, string $savePath)
    {
        $wkHtmlToPdf = new Pdf($this->generatePdfOptions($app));

        $urlToPdfConverter = new PhpWkHtmlToPdfConverter(
            $url,
            $savePath,
            $wkHtmlToPdf
        );

        try {
            if (true === $this->validateUrlToPdfConverter($app, $urlToPdfConverter)) {
                $urlToPdfConverter->execute();
            }
        } catch (\Exception $e) {
            return new Response(
                $e->getMessage(),
                Response::HTTP_INTERNAL_SERVER_ERROR,
                ['content-type' => 'text/plain']
            );
        }

        return $this->streamFile($app, $savePath);
    }

    /**
     * Generate default options for PHP WkHtmlToPdf.
     *
     * @param Application $app
     *
     * @return array
     */
    private function generatePdfOptions(Application $app)
    {
        // default options
        $options = [
            'no-outline',
            'margin-top' => 0,
            'margin-right' => 0,
            'margin-bottom' => 0,
            'margin-left' => 0,
            'disable-smart-shrinking',
        ];

        // ignore warnings when in prod environment
        if (false === $app['debug']) {
            $options['ignoreWarnings'] = true;
        }

        return $options;
    }

    /**
     * Validate the UrlToPdfConverter.
     *
     * @param Application       $app
     * @param UrlToPdfConverter $urlToPdfConverter
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function validateUrlToPdfConverter(
        Application $app,
        UrlToPdfConverter $urlToPdfConverter
    ) {
        $errors = $app['validator']->validate($urlToPdfConverter);

        if (count($errors) > 0) {
            $errorMessages = '';
            foreach ($errors as $error) {
                $errorMessages .= ('' === $errorMessages ? '' : "\n").$error->getMessage();
            }

            throw  new \Exception($errorMessages);
        } else {
            return true;
        }
    }

    /**
     * Stream the file.
     *
     * @param Application $app
     * @param string      $filePath
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    private function streamFile(Application $app, string $filePath)
    {
        $stream = function () use ($filePath) {
            readfile($filePath);
        };

        return $app->stream($stream, 200, ['Content-Type' => 'application/pdf']);
    }
}
