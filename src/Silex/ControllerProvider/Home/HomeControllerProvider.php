<?php

namespace ArashDastafshan\PdfGeneratorApi\Silex\ControllerProvider\Home;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

class HomeControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        // creates a new controller based on the default route
        $controllers = $app['controllers_factory'];

        $controllers->get('/', function (Application $app) {
            $response = new Response(
                $this->getContent(),
                Response::HTTP_OK,
                ['content-type' => 'text/plain']
            );

            $response->setCharset('UTF-8');

            return $response;
        });

        return $controllers;
    }

    /**
     * The content for the homepage.
     *
     * @return string
     */
    private function getContent()
    {
        return
            "This is a coding example for a PDF generator API written by Arash \n".
            "Dastafshan using principles like SOLID, TDD and BDD.\n".
            "\n".
            "The application is built on top of the Silex micro-framework. Tools \n".
            "like PHPUnit and Behat are used to make automated tests possible. To\n".
            "make the code more modular, the code has been separated into\n".
            "different custom Composer packages. The PDF generator is built on\n".
            "top of an existing WkHtmlToPdf wrapper.\n".
            "\n".
            "The goal of this example is to show how you can write readable,\n".
            "maintainable and reusable code that can also be automatically\n".
            "tested.\n".
            "\n".
            "Only one action is possible in this example.\n".
            "\n".
            "To convert a URL to a PDF, perform the following GET action:\n".
            "\n".
            '/url-to-pdf?url=example.com';
    }
}
