<?php

namespace ArashDastafshan\PdfGeneratorApi\Silex\Tests\Scenario\Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Igorw\Silex\ConfigServiceProvider;
use Silex\Application;
use Silex\Provider\ValidatorServiceProvider;

class App implements Context, SnippetAcceptingContext
{
    /**
     * @var Application
     */
    protected $app;

    public function __construct()
    {
        $this->app = new Application();

        $basePath = dirname(__DIR__, 5);

        // load global config files
        $this->app->register(new ConfigServiceProvider($basePath.'/config/global/default.json'));
        $this->app->register(new ConfigServiceProvider($basePath.'/config/global/scenariotesting.json'));

        // load local config files (these need to be created first)
        if (file_exists($basePath.'/config/local/default.json')) {
            $this->app->register(new ConfigServiceProvider($basePath.'/config/local/default.json'));
        }
        if (file_exists($basePath.'/config/local/scenariotesting.json')) {
            $this->app->register(new ConfigServiceProvider($basePath.'/config/local/scenariotesting.json'));
        }

        // load validators
        $this->app->register(new ValidatorServiceProvider());
    }
}
